---
layout: markdown_page
title: "GitLab Direction"
---

This page describes the direction and roadmap for GitLab.
It is organized from the short to the long term.

## Your contributions

GitLab's direction is determined by the code that is sent by our [contributors](http://contributors.gitlab.com/).
We continually merge code to be released in the next version.
Contributing is the best way to get a feature you want included.
On [our issue tracker for CE](https://gitlab.com/gitlab-org/gitlab-ce/issues)
and [EE](https://gitlab.com/gitlab-org/gitlab-ee/issues),
many requests are made for features and changes to GitLab.
The ones with the
[status accepting merge requests](https://gitlab.com/gitlab-org/gitlab-ce/issues?milestone_id=&scope=all&sort=created_desc&state=opened&utf8=%E2%9C%93&assignee_id=&author_id=&milestone_title=&label_name=Accepting+Merge+Requests&weight=)
are pre-approved.
Of course before any code is merged it still has to meet the
[contribution acceptance criteria](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria).

## What our customers want

At GitLab the company we try to make what our users and we need (many of us are or used to be developers).
If a customer requests a feature, it carries extra weight.
Due to our short release cycle we can ship simple feature requests (for example an API extension) within one to two months.

## Previous releases

On our [release list page](https://about.gitlab.com/release-list/) you can find an overview of the most important features of recent releases and a links to the release blog posts.

## Next releases

GitLab releases a new version every single month on the 22nd.
Note that we often move things around, do things that are not listed and don't do things that are listed.
This page is always in draft, some of the things on it might not ever be in GitLab.
EE options are indicated with 'EE option' in the issue title, this is our best estimate of what will be options, it is not definitive.
Also the list below not include any contributions from outside GitLab the company.
The bullets list the tentpole features; the most important features of upcoming releases.
The CE and EE to the right of the version number link to all planned issues for that version.

<%= direction %>

## Wishlist <a name="wishlist"></a>

Below are features we'd really like to see in GitLab.
This list is not prioritized. We invite everyone to join the discussion by clicking the wishlist item that is of interest to you.
Feel free to comment, vote up or down any issue or just follow the conversation.  For GitLab sales, please add a link to the account in Salesforce.com that has expressed interest in a wishlist feature.
We very much welcome contributions that implement any of these things.

### Major Wins

- [Translations](https://gitlab.com/gitlab-org/gitlab-ce/issues/4012)
- [Nested Groups](https://gitlab.com/gitlab-org/gitlab-ce/issues/2772)
- [Resolve merge conflicts in the web interface](https://gitlab.com/gitlab-org/gitlab-ce/issues/3567)
- [Multithreaded application server](https://gitlab.com/gitlab-org/gitlab-ce/issues/3592)
- [Realtime editing of the issue/MR description field](https://gitlab.com/gitlab-org/gitlab-ce/issues/4199)
- [Handle incoming emails with support questions in issues](https://gitlab.com/gitlab-org/gitlab-ee/issues/149)
- [Cherry-pick MR into any branch in the web interface](https://gitlab.com/gitlab-org/gitlab-ce/issues/12785)
- [Allow pusing to multiple servers](https://gitlab.com/gitlab-org/gitlab-ee/issues/276)

### Usability

- [See all forked projects of one project](https://gitlab.com/gitlab-org/gitlab-ce/issues/2406)
- [Email actions](https://gitlab.com/gitlab-org/gitlab-ce/issues/4273)
- [Improved emails on push](https://gitlab.com/gitlab-org/gitlab-ee/issues/146)
- [Autocomplete all users](https://gitlab.com/gitlab-org/gitlab-ce/issues/3872)
- [Ship octotree as part of GitLab](https://gitlab.com/gitlab-org/gitlab-ce/issues/13723)

### Code Review

- [See current HEAD in outdated discussion](https://gitlab.com/gitlab-org/gitlab-ce/issues/3502)
- [Allow cross server merge requests](https://gitlab.com/gitlab-org/gitlab-ce/issues/4013)
- [Ability to checkout the result of MR available before merging](https://gitlab.com/gitlab-org/gitlab-ce/issues/2765)
- [Transactional MR comments](https://gitlab.com/gitlab-org/gitlab-ce/issues/3364)

### Issue tracker

- [Filter by more than one label](https://gitlab.com/gitlab-org/gitlab-ce/issues/989)
- [Time tracking](https://gitlab.com/gitlab-org/gitlab-ee/issues/78)
- [Labels should be visible in Milestone view](https://gitlab.com/gitlab-org/gitlab-ce/issues/3276)

### Productivity

- [Change notification setting](https://gitlab.com/gitlab-org/gitlab-ce/issues/3778)
- [Add more default views](https://gitlab.com/gitlab-com/www-gitlab-com/edit/master/source/direction/index.md)

### Version Control for Everything

- [Distributed code-reviews and issues](https://gitlab.com/gitlab-org/gitlab-ce/issues/4084)
- [Render PSD files](https://gitlab.com/gitlab-org/gitlab-ce/issues/13189)
- [Snippets backed by a git repository to enable cloning and revisions](https://gitlab.com/gitlab-org/gitlab-ce/issues/13426)
- [Binary file lock (EE option)](https://gitlab.com/gitlab-org/gitlab-ce/issues/7889)

### Performance

- [Handle errors before they give a 500](https://gitlab.com/gitlab-org/gitlab-ce/issues/4665)
- [Get rid of the large authorized keys (SSH) file](https://gitlab.com/gitlab-org/gitlab-git-http-server/issues/2#note_1983654)
- [Participants table](https://gitlab.com/gitlab-org/gitlab-ce/issues/3965)
- [Reduce memory leaks](https://gitlab.com/gitlab-org/gitlab-ce/issues/3700)

### CI

- [Runner Autoscale](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/318)
- [Document dependent builds / pipeline triggers](https://gitlab.com/gitlab-org/gitlab-ce/issues/3432)
- [Deploy your branch button](https://gitlab.com/gitlab-org/gitlab-ce/issues/3286)
- [Automatically give code quality metrics in the merge request](https://gitlab.com/gitlab-org/gitlab-ce/issues/4044)
- [Automatic Docker image cleanup](https://gitlab.com/ayufan/gitlab-runner-docker-cleanup/issues/1)
- [Show code coverage in diffs with colored horizontal bar](https://gitlab.com/gitlab-org/gitlab-ce/issues/4073)
- [A/B testing of branches with GitLab Pages (EE option)](https://gitlab.com/gitlab-org/gitlab-ee/issues/117)
- [Code coverage graphs and adding more metrics (EE option)](https://gitlab.com/gitlab-org/gitlab-ce/issues/13409)
- [Pre-testing of merged results (EE option?)](https://gitlab.com/gitlab-org/gitlab-ce/issues/4176)
- Feature deploy (measure KPI and use feature toggles) (EE option)
- Automatic environment per merge request (EE option)
- Automatic A/B testing of branches (EE option)
- Configuration management integration (Chef, Puppet, Ansible, Salt) (EE option)

### Access control

- Directory access management (EE option)
- Turnstile security that is self learning and location dependent (EE option)
- Automated vulnerability scanner (EE option)

### Tracebility

- Watermarking of binaries that are downloaded (EE option)
- Build traceability (trace binary to source) (EE option)
- New code license detection (scans the internet) (EE option)

### Analytics

- Advanced compliance reporting on access rights (EE option)
- Advanced analytics features (EE option)

### Reporting

- Aggregated time reports (time spent per project, etc.) (EE option)
- Management dashboard (time spend per phase, cycle time and velocity) (EE option)

### Scope <a name="scope"></a>

[Our vision](#vision) is the need for an integrated set of tools for the software development lifecycle based on convention over configuration.
To achieve this we plan to ship the following stack of tools in our Omnibus package:

1. **Chat** conversation => Mattermost, [Rocket.Chat](http://rocket.chat/) if they can make it [work with PostgreSQL](https://github.com/RocketChat/Rocket.Chat/issues/533)
1. **Issue** creation => GitLab Issues
1. **Scrum** board to plan => [Huboard](https://huboard.com/) for which there is a [bounty](https://github.com/huboard/huboard/issues/276)
1. **Development** => [Browser IDE](https://gitlab.com/gitlab-org/gitlab-ce/issues/12759)
1. **Version control** => GitLab Repo
1. **Continuous integration** => GitLab CI
1. **Code review** => GitLab Merge Requests
1. **Deploy** to production => [GitLab Deploy](https://gitlab.com/gitlab-org/gitlab-ce/issues/3286)
1. **Chatops** to check => [Hubot](https://hubot.github.com/) which has a [Merge Request](https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests/564) or [Lita](https://www.lita.io/)
1. **Wiki** to document => GitLab Wiki

We're still deciding if superficial application performance (APM) monitoring is in our scope.
We don't want to do deep APM since that sophisticated requires introspection of code.
But we are considering shipping Grafana and InfluxDB as part of our Omnibus package.
Examples of the advantages of adding APM to GitLab would be:

- No external tools or setup needed for [GitLab performance monitoring](http://doc.gitlab.com/ce/monitoring/performance/introduction.html).
- It would make it easier have our [deployment pipeline](https://gitlab.com/gitlab-org/gitlab-ce/issues/750910) automatically revert a deployment when it impacts performance.
- We can use it to visualize the outcome of [A/B testing](https://gitlab.com/gitlab-org/gitlab-ee/issues/117).

Things that are outside our scope are:

1. **PaaS** although we do want to use [GitLab Deploy](https://gitlab.com/gitlab-org/gitlab-ce/issues/3286) to deploy to CloudFoundry, OpenStack, OpenShift, Kubernetes, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Configuration management** although we do want to upload cookbooks, manifests, playbooks, and modules for respectively Chef, Puppet, Ansible, and Salt.
1. **Distributed configuration stores** Consul, Etcd, Zookeeper, Vault
1. **Container configuration agents** [ContainerPilot](http://container-solutions.com/containerpilot-on-mantl/) [Orchestrator-agent](https://www.percona.com/blog/2016/04/13/orchestrator-agent-how-to-recover-a-mysql-database/)
1. **Log monitoring** ELK stack, Graylog, Splunk
1. **Infrastructure monitoring** CheckMK, Nagios, Sensu
1. **Error monitoring** Sentry, Airbrake, Bugsnag
1. **Network** Openflow, VMware NSX, Cisco ACI
1. **Incident notification** Pagerduty, Pingdom
1. **Network security** Nmap, rkhunter, Metasploit, Snort, OpenVAS, OSSEC

### We would love to partner with

- Scrum boards: We already support [some of them](https://about.gitlab.com/applications/#scrum-boards) but would love for [Waffle.io](https://waffle.io/), and [Zenhub.io](https://www.zenhub.io/) to support GitLab too.
- [CodeClimate](https://gitlab.com/gitlab-org/gitlab-ce/issues/4044)
- [Oracle database support (EE)](https://gitlab.com/gitlab-org/gitlab-ee/issues/96)
- Browser IDE's to ship GitLab with them and to have button to open them from GitLab: Koding, Nitrous.io, [Cloud9](https://c9.io/blog/cloud9-template-days/), CodeAnywhere, Codio, and CodeEnvy

## Vision <a name="vision"></a>

From development teams to marketing organizations, everyone needs to collaborate on
digital content. Content should be open to suggestions by a wide number of
potential contributors. Open contribution can be achieved by using a mergeable
file format and distributed version control. The vision of GitLab is to **allow
everyone to collaborate on all digital content** so people can cooperate
effectively and achieve better results, faster.

Ideas flow though many stages before they are realized. An idea originates in a chat discussion, an issue is created, it is planned in a sprint, coded in an IDE, committed to
version control, tested by CI, code reviewed, deployed, checked and documented. Stitching all these stages of the software developement lifecycle together can be done in many different ways. You can have a marketplace of
proprietary apps from different suppliers or use a suite of products developed
in isolation. We believe that an **integrated set of tools for the software development lifecycle based on convention over configuration** offers a superior user experience. The
advantage can be quoted from the [Wikipedia page for convention over
configuration](https://en.wikipedia.org/wiki/Convention_over_configuration):
"decrease the number of decisions that developers need to make, gaining
simplicity, and not necessarily losing flexibility". In GitLab you only have to
specify unconventional aspects of your workflow.
The happy path is **frictionless from idea to production**.

We admire other convention over configuration tools like [Ruby on
Rails](http://rubyonrails.org/) (that doctrine of which perfectly describe the [value of integrated systems](http://rubyonrails.org/doctrine#integrated-systems), [Ember](http://emberjs.com/), and
[Heroku](https://www.heroku.com/), and strive to offer the same advantages for a
continuous delivery of software.

We prefer to offer an integrated set of tools instead of a network of services or offering plugins for the following reasons:

1. We think an integrated set of tools provides a better user experience that a modular approach, as detailed by [this article from Stratechery](https://stratechery.com/2013/clayton-christensen-got-wrong/).
1. The open source nature of GitLab ensures that that we can combine great open source products.
1. everyone can contribute to a create a feature set that is [more complete than other tools](https://about.gitlab.com/comparison/). We'll focus on making all the parts work well together to create a better user experience.
1. Because GitLab is open source the enhancements can become [part of
the codebase instead](http://doc.gitlab.com/ce/project_services/project_services.html) of being external. This ensures the automated tests for all
functionality are continually run, ensuring that additions keep working work. This is contrast to externally maintained plugins that might not be updated.
1. Having the enhancements as part of the codebase also
ensures GitLab can continue to evolve with it's additions instead of being bound
to an API that is hard to change and that resists refactoring. Refactoring is essential to maintaining a codebase that is easy to contribute to.
1. Many people use GitLab on-premises, for such situations it is much easier to install one tool than installing and integrating many tools.
1. GitLab is used by many large organizations with complex purchasing processes, having to buy only subscription simplifies their purchasing.
