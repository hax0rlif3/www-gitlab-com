---
layout: markdown_page
title: Team Structure
page_class: org-structure
---

Draft team structure, this is to be finished after the Austin Summit (end of May 2016).

GitLab Inc. has at most four layers in the team structure:

1. CEO
1. Executives (e-team) consisting of CxO's and VP's
1. Leads
1. Individual contributors (IC's)

The indentation below reflects the reporting relations.
You can see our complete team and who reports to who on the [team page](https://about.gitlab.com/team/).
If there is a hyphen (-) in a line the part before hyphen is the name of the department and sometimes links to the relevant part of the [handbook](https://about.gitlab.com/handbook/).
The job titles link to the job descriptions.
Our open vacancies are at our [jobs page](https://about.gitlab.com/jobs/).


- [General](/handbook/) - [CEO](/jobs/chief-executive-officer/)
  - [Marketing](/handbook/marketing/) - [CMO](/jobs/chief-marketing-officer/)
    - Design - [Designer](/jobs/designer/)
    - [Demand generation](/handbook/marketing/demand-generation) - [Senior Demand Generation Manager](/jobs/demand-generation-manager/) (lead)
      - [Online Marketing](/handbook/marketing/online-marketing) - [Online Marketing Manager](/jobs/online-marketing-manager/)
      - Business Development - [Business Development Team Lead](/jobs/business-development-team-lead/)
      - [Business Development Representatives](/jobs/business-development-representative/)
    - [Product Marketing](/handbook/marketing/product-marketing/) - [Senior Product Marketing Manager](/jobs/product-marketing-manager/) (lead)
      - [Partner Marketing](/handbook/marketing/product-marketing/#partnermarketing/) - Partner/Channel Marketing Manager (vacancy)
      - [Content Marketing](/handbook/marketing/developer-relations/content-marketing/) - Content Marketing Manager (vacancy)
    - Developer Relations
      - [Developer Advocacy](/handbook/marketing/developer-relations/developer-advocacy/) - [Developer Advocate](/jobs/developer-advocate/)
      - [Field Marketing](/handbook/marketing/developer-relations/field-marketing/) - [Field Marketing Manager](/jobs/field-marketing-manager/)
      - Technical Writing - [Technical Writer](/jobs/technical-writer/)
  - [Sales](/handbook/sales-process/) - [CRO](/jobs/chief-revenue-officer/)
    - Americas Sales - [Account Executive](/jobs/account-executive/)
    - EMEA Sales - [Sales Director EMEA](/jobs/sales-director/) (lead)
      - [Account Executive](/jobs/account-executive/)
    - APAC Sales - [Director of Global Alliances and APAC Sales](/jobs/director-of-global-alliances-and-apac-sales/) (lead)
    - Customer Success - Customer Success Manager (lead, future vacancy)
      - [Account Manager](/jobs/account-manager/)
      - [Solutions Engineer](/jobs/solutions-engineer/)
  - Finance - [CFO](/jobs/chief-financial-officer/)
    - [Accounting](/handbook/accounting/) - [Controller](/jobs/controller/)
    - [People Operations](/handbook/people-operations/) - [People Operations Director](/jobs/people-ops-director/) (lead, vacancy)
      - [People Operations Coordinator](/jobs/people-ops-coordinator/)
      - [Administrative Coordinator](/jobs/adminstrative-coordinator/)
  - [Technical Direction](/direction/) - [CTO](/jobs/chief-technology-officer/)
  - Engineering - [VP of Engineering](/jobs/vp-of-engineering/)
    - Backend - [Backend Lead](/jobs/backend-lead/)
      - [Developers](/jobs/developer/) that are Rails specialists
    - Frontend - [Frontend Lead](/jobs/frontend-lead/)
      - [Frontend Engineers](/jobs/frontend-engineer/)
    - [Infrastructure](/handbook/operations/) - Infrastructure lead
      - [Production Engineers](/jobs/production-engineer/)
      - [Developers](/jobs/developer/) that are a performance specialist
    - [Support](/handbook/support/) - Support lead
      - [Service Engineers](/jobs/service-engineer/)
    - [UX Designers](/jobs/ux-designer/)
    - [Developers](/jobs/developer/) that are maintainers
    of or specialist in projects without a lead
    - [Developers](/jobs/developer/) that are a merge request coach
  - Scaling - [VP of Scaling](/jobs/vp-of-scaling/)
  - General Product - [VP of Product](/jobs/vice-president-of-product/)
  - CI/CD Product - [Head of Product](/jobs/head-of-product/)
