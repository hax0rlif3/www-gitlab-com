---
layout: markdown_page
title: "Hiring"
---
## On this page:

* [Vacancy Creation Process](#vacancy-creation-process)
* [Advertising the job description](#job-advertising)
* [Hiring Process](#hiring-process)
* [Screening Call](#screening-call)
* [Interviewing](#interviewing)
* [Interview Questions](#interview-questions)
* [Reference calls](#reference-calls)
* [Getting Contracts Ready, Reviewed, and Signed](#prep-contracts)
* [Underperformance](#underperformance)

## Vacancy Creation Process<a name="vacancy-creation-process"></a>

The CEO needs to authorize any new job positions/searches, and agree on the proposed hiring team.

1. Define hiring team. Roles can be assigned fluidly (see below for the [Hiring Process](#hiring-process)), depending on who is available, bearing in
mind that the most time consuming aspect tends to be review of the first wave of applicants.
1. Create the job description on our website, and in Workable
    1. Create the relevant page in `https://about.gitlab.com/jobs/[name-of-job]`
    1. Add a job of the exact same job title on [Workable](https://gitlab.workable.com/backend)
       * For location, select "Telecommute".
       * For the description, simply write `For the job description, see [url of relevant jobs page on GitLab's website]`
       * Indicate what applicants need to provide with their application. By default, this will include their resumé, a cover letter, but it may also
       include qualifying questions such as "What timezone are you in?" and "Are you aware that this is not a DevOps role?".
       * "Publish" the job, and follow the links to the application form.
    1. Embed the link to the application form for the new job on our [Jobs page](https://about.gitlab.com/jobs/), and also include a link on their to the job description. As soon as the new job description / posting is "live" on our website, also consider your available [advertising methods](#job-advertising) below.


## Advertising the job<a name="job-advertising"></a>

_Always_ advertise the job through the following means:

1. "Soft” referral, e.g. all GitLab staff post link to jobs site on their LinkedIn profiles.
1. Tweet the new job posting.
1. Use the [Workable Clipper](http://resources.workable.com/the-workable-clipper) to help source candidates directly from LinkedIn, and  familiarize yourself with the Workable environment, work flow, features, and support desk.

_Consider_ advertising the job through the following means:

List on:
1. [Hacker News](https://news.ycombinator.com/ask): On the first of the month, include a note for GitLab in the Hacker News thread of "Who's Hiring" . Template text: `REMOTE ONLY GitLab
We're hiring production engineers, developers, UX designers , and more. https://about.gitlab.com/jobs/ We're a remote only company so everyone is on an equal level. GitLab is a Ruby on Rails project with over 1000 contributors.`
1. [WeWorkRemotely](https://weworkremotely.com) ($200 for 30 days, per position).
1. [RemoteOK](https://remoteok.io) ($200 for ?? days, per position)



## Hiring Process<a name="hiring-process"></a>

1. Confirm application: applicants automatically receive confirmation of their application, thanking them for submitting their information. This is an automated message from Workable. If the person came though another channel please add them to Workable before continuing the process. There are various ways to do this, see [Workable's documentation](https://resources.workable.com/adding-candidates).
1. Ask more information if needed: if information is missing and the applicant seems sufficiently promising (or not enough information to be able to make that determination), the appropriate person from the hiring team should follow up requesting additional information.
1. Hiring manager does a first round of rejections. Disqualified candidates should be sent a note informing them of the rejection. There are templates in Workable to assist, but messages can be tailored as appropriate: place yourself in the receiving end of the message.
1. [Screening call](#screening-call): in Workable, leave a note for our [administrative coordinator](https://about.gitlab.com/jobs/administrative-coordinator/) to ping for a screening call; and also move the applicant to the "screening call" stage in Workable. Depending on the outcome of the screening call, the hiring manager can either [reject an applicant](#rejecting), or move the applicant to the interview stage in Workable.
1. Technical interview (optional): As described on the [Jobs](https://about.gitlab.com/jobs/) page, certain positions
require [technical interviews](https://about.gitlab.com/jobs/#technical-interview).
1. Manager interview (see below for questions)
1. For female candidates, include at least one interview with a female GitLab team member.
1. C-level executive interview (if different than the manager, see below for questions)
1. CEO interview (if different than the C-level executive, see below for questions)
1. Make [reference calls](#reference-calls) for promising candidates. This process can start at an earlier stage, but should happen before an offer is made. Move the candidate to the "Reference Call" stage in Workable, and ping the relevant person from People Ops to get it going.
1. Make a verbal or written (email) offer (the CEO needs to authorize offers)
1. Hiring manager follows up to ensure that the offer is accepted, and then moves to [preparing contracts](#prep-contracts)
1. Hiring manager ensures that the contract is signed, and [starts the onboarding process](#move-to-onboarding) (the People Ops team can help).

### Moving applicants to the next step

If you have decided to let the applicant move on to the next interview, feel free to send the next interviewer's calendly link to the applicant.
This saves the time that it would otherwise take for the next interviewer to get around to sending the link, and it thus works in everyone's benefit.

### Interviewing

Interviewing is hard, it's hard for both sides. In less than one hour you both need to get to know each other and
both will have to make the decision if you want to work with this person.
This is an effort to provide a set of guidelines to make interviewing a bit less traumatizing.


#### Before the interview

* Screening - writing a good resume is an art, and not many people master it. When you read a resume look for evolution rather than buzzwords, and if something sparks your curiosity, ask.
* Spend some time talking through email to get to know each other. Since GitLab is remote-only company, most of our interactions happen on written form. Check that this person can communicate efficiently this way. If it is for a technical position, ask open ended technical questions. The value of these questions is that there is no absolute good answer, it lets you see how this person thinks.
* If the process is taking too long, apologize and explain what is going on. It is really frustrating to not hear anything from the other side, and then resume conversations like nothing has happened. Show respect for the time of the candidate.
* If the process ends at this stage, be kind, and if the interviewee asks for feedback, give honest constructive feedback that explains why have we taken our decision.

#### During the interview:

1. There is an unbalanced power relationship during the interview. The interviewer is in a powerful position. It will decide if this candidate will get an offer or not. Be mindful of this. Be as friendly and approachable as you can. Be frank about what is going on, explain how the interview is going to be and set clear expectations: tell it like it is. This has the added value of getting people comfortable (over time) and allows you to get much better data.
1. Communication is really hard, don't expect perfect answers. Every person is different and they will express things differently, they are not listening your train of thought so they will say things differently than what you expect, work on approaching to what they are trying to say rather than demanding them to approach to you. Once you have an answer validate your assumptions by explaining to the interviewed what did you understood and allow the candidate to correct your story.
1. Don't go checking for perfect theoretical knowledge that the interviewee can google when needed, or give a problem that took you 2 months to dominate yet you expect your interviewee to master in a 30 minutes conversation. Be fair.
1. Aim to at the end of the interview know if you want to work with this person.
1. Interview for softskills, really, do it. Pick some behavioural questions to get data on what has the candidate done before and how his behaviour aligns to the company values. We are all going to be much happier if we all naturally agree on how things should be.
1. Consider having more people interviewing with you, different people see and value different things. More data helps making better decisions and ends up being a better use of interviewing time for both the candidate and the company.
1. Always let the interviewee ask questions at the end, and be frank in your answers.

##### Technical interviews

1. Try to get a real sample of work (which we already do for developers by working on GitLab issues) Avoid puzzles or weird algorithm testing questions. Probing for datastructures is fine as long as it is relevant to the job the person is going to do.
1. Be mindful of the background of the candidate, someone who knows 10 languages already (and some languages in particular, Perl for ex), may pickup ruby in a second given the right chance. Don't assume that someone with a Java background will not be capable of moving to a different stack.
1. Consider including non technical people performing soft skills questions. Because technical people should be capable of talking to non-technical just fine, we should assess it.

#### Behavioural questions (STAR)

The goal of these questions is to get the candidate to share something they did in the past. Previous behaviour is considered the best way to predict how a person is going to act in the future.

They usually start with the form "Can you tell me about a time when...". The kind of answer that we are looking for is to get a story that is structured following the Situation, Task, Action, Result.

There is no right answer, what matters here is to hear the candidate and gather data on how is it telling the story.

Some things to pay attention to:
* What was the candidate role? Was it a leader? A follower? Why?
* What is it highlighting as important? Did it matter?
* Is it clearly explained? Is the story well told? If it is a technical story and the interviewer is a non-technical person, are things being explained in a way that make sense?
* If you ask "why?" will you get an actual explanation?
* Is the Task and the Action clear? Is it well reasoned?
* Is there a result or was the story left unfinished? Is it still going on?
* Was the result measured in any way? How does the candidate know that the result matches the expectation? Was there an expectation?

These questions can be quite unbalancing and can increase the stress during the interview. Again, be kind and help the candidate understand what are you looking for, provide a sample if it is needed and you notice that the candidate is blocked.

It can also happen that the candidate does not have a story to share with you, that is OK. It's just another data point that should be added to the feedback (I failed to get data on ...), just move to the next question, just be sure to have a few questions as a backup.

These questions should be aligned with our company values. What we are looking for is understanding how this candidate behaves, and if this behaviour matches the one we look for in our company values.

Once you have your notes, tell the candidate what you understood, repeat the story, and let them correct you as needed.

#### Interview feedback

Always leave feedback, this will allow the rest people to understand what happened and why you took the decision you took.

One way of writing the feedback is as follows:
```
Vote: inclined/not inclined

Summary: your general impressions, a brief description on what have you seen, where you stand, and why.

Pros: what is good, and where did you found it during the interview, bullet points is fine.

Cons: weak spots. Where the candidate failed to you, why, consider that some things can be taught or learnt.

Interview notes: What questions were asked, what story you got back. Ex.

  Tell me about a time when you did X

  The candidate told me a story when she was working in his current gig doing... They had to do... It went well because she took the leadership and .... In the end they increased their sales in a 30%, she measured it by doing ...
```

The vote is critical as it is telling the rest of the people what is your final thinking here. The rest should explain why do you think so.

### Rejecting applicants<a name="rejecting"></a>

1. At any time during the hiring process the applicant can be rejected
1. The applicant should always be notified of this. The hiring manager is primarily
responsible for this, but People Ops can help and does a weekly check-up in Workable.
1. If the applicant asks for further feedback always offer frank feedback. This
is hard, but it is part of our company values.
1. If the candidate is not hired, People Ops sends out an email to ask for feedback.
There is a "gathering applicant feedback" template in Workable with these questions.
The feedback survey should be sent out about 3 days after the applicant has been
notified of the rejection.
1. PeopleOps will receive the feedback and will use this to improve the hiring process.


## Screening Call<a name="screening-call"></a>

For some positions, we conduct screening calls. This call is typically done by our [administrative coordinator](https://about.gitlab.com/jobs/administrative-coordinator/).

Questions are:

1. Why are they looking for a new job?
1. What is your experience with X? (do for each of the skills asked in the job description)
1. How do they feel about working remotely and do they have experience with it?
1. Compensation expectation and compensation in current/last job.

[An example of the output of a good screening call](https://gitlab.workable.com/backend/jobs/128446/browser/applied/candidate/7604850) (need workable account).

At the end of the screening call applicant should be told what the timeline is for what the next steps are (if any).
An example message would be "We are reviewing applications through the end of next week, and will let you know by the end of two weeks from today whether you've been selected for the next round or not. Please feel free to ping us if you haven't heard anything from us by then."

## Interview Questions<a name="interview-questions"></a>

Note: So you are about to interview folks for a job at GitLab? Please take a moment to carefully read
[this document on keeping it relevant and legal, including a self-test](https://docs.google.com/document/d/1JNrDqtVGq3Y652ooxrOTr9Nc9TnxLj5N-KozzK5CqXw).

1. Do you have any questions about the job, our organization, our strategy, or me personally?
1. Why did you apply to GitLab?
1. For each significant study and job I will ask: why did you select this one and why you moved on? Please give a short answer in 1 or 2 sentences. I will ask if I want to know more. You don't have to say what you did during the job, you already passed your technical interview.
1. What where you most satisfied with in your recent/current position?
1. What did you like least about your recent/current position?
1. Take each skill required for the job and do a [STAR](https://en.wikipedia.org/wiki/Situation,_Task,_Action,_Result) for a couple of situations.
1. What professional achievements are you most proud of?
1. How do you keep up to date with developments in your profession?
1. If you don't get this job what will you do?
1. Are you interviewing anywhere else?
1. How can we change GitLab the software product to make it better?
1. What can we change in GitLab the organization to make it better, for example the hiring process or the handbook?
1. What do you expect to achieve in your first month at GitLab?
1. Where do you want to be in three years from now?
1. How do you feel about working remote?
1. If you get hired when can you start?
1. What compensation would you feel comfortable with?
1. Do you have any questions for me?

## Reference calls <a name="reference-calls"></a>

As part of our hiring process we may ask applicants to provide us with one or more
references to contact. These reference calls are typically be done by our [administrative coordinator](https://about.gitlab.com/jobs/administrative-coordinator/) or the hiring
 manager for that specific vacancy.

## Getting Contracts Ready, Reviewed, and Signed<a name="prep-contracts"></a>

Offers made to new team members should be documented in the email thread between the
person authorized to make the offer (e.g. CEO) and the applicant.

1. Email example:
```
We would love to have you as part of our team. You will have
a position of [job title]. As a contractor you will invoice [monthly rate] per month.
You will report to [name of manager]. We proposed to make you eligible for [number] of stock options.
We will send you a contract on [day of week] based on
the examples on https://about.gitlab.com/handbook/contracts/. If you have not
received anything on that day please reply-to-all to let us know. Please let us
know if you have any questions or concerns in the meantime.
```
1. In the email confirmation of the offer,
put the People Operations email alias (which can be found in the "GitLab Email Forwarding" google doc) in the cc.
1. One person from People Operations will reply-to-all to everyone in the thread
(including the applicant) to confirm that they will make the contract. Speed matters: if you are in People Operations and you can
tackle this, then raise your hand and hit reply-all.
   - The VP of Scaling reviews the contract
after which it can be sent out to be signed by the C-level team
member that made the offer.
1. This person from People Operations makes the contract based on the details found in the Workable
platform, and uses reply-all to gather any missing pieces of information, and to
confirm with a reply-to-all when the contract is sent. Note: the number of proposed stock options
must always be mentioned specifically, even when it is 0. People Ops must follow-up
with the person who requested the contract to make sure that this point is addressed.
1. This same person from People Operations files the signed contract in the appropriate place, and starts the [**onboarding issue**](https://about.gitlab.com/handbook/general-onboarding/).

Note for People Operations:
- the type of contract required (employee or contractor; BV or Inc) is clarified by the guideline on the
[Contracts page](https://about.gitlab.com/handbook/contracts).
- Onboarding info for the PeopleOps system, BambooHR, can be found on the [PeopleOps](about.gitlab.com/handbook/people-operations) page.

## Underperformance <a name="underperformance"></a>

See [underperformance guidelines](/handbook/hiring/underperformance.html).
